install: xmltopdf
	ln -srf ./xmltopdf ~/bin/xmltopdf

.PHONY: clean

clean:
	rm ~/bin/xmltopdf
